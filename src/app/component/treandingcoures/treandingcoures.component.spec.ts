import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreandingcouresComponent } from './treandingcoures.component';

describe('TreandingcouresComponent', () => {
  let component: TreandingcouresComponent;
  let fixture: ComponentFixture<TreandingcouresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreandingcouresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreandingcouresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
