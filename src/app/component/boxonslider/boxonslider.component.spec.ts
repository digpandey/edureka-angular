import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxonsliderComponent } from './boxonslider.component';

describe('BoxonsliderComponent', () => {
  let component: BoxonsliderComponent;
  let fixture: ComponentFixture<BoxonsliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxonsliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxonsliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
