import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SliderComponent } from './component/slider/slider.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { BoxonsliderComponent } from './component/boxonslider/boxonslider.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { TreandingcouresComponent } from './component/treandingcoures/treandingcoures.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    NavbarComponent,
    BoxonsliderComponent,
    SidebarComponent,
    TreandingcouresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
